AWSTemplateFormatVersion: 2010-09-09
# This CloudFormation template deploys a basic VPC / Network. 
# This is also my first YML file in my life.
Parameters:
  RDSUsername:
    Type: String
  RDSPassword:
    Type: String
  RDSDBName:
    Type: String
  RDSHostName:
    Type: String
  DBAllocatedStorage:
    Description: the number of the volume for db
    Type: String
    Default: 8
  DBInstanceClass:
    Description: my DBinstanceTyoe
    Type: String
    Default: db.t2.micro
  AmiId: 
    Description: my AMI id for ec2 instance building.
    Type: String
  KeyName:
    Description: Name of an existing EC2 KeyPair to enable SSH access to the instances
    Type: 'AWS::EC2::KeyPair::KeyName'
    ConstraintDescription: must be the name of an existing EC2 KeyPair.
  InstanceType:
    Description: WebServer EC2 instance type
    Type: String
    Default: t2.small
    AllowedValues:
      - t1.micro
      - t2.nano
      - t2.micro
      - t2.small
      - t2.medium
      - t2.large
      - m1.small
      - m1.medium
      - m1.large
      - m1.xlarge
      - m2.xlarge
      - m2.2xlarge
      - m2.4xlarge
      - m3.medium
      - m3.large
      - m3.xlarge
      - m3.2xlarge
      - m4.large
      - m4.xlarge
      - m4.2xlarge
      - m4.4xlarge
      - m4.10xlarge
      - c1.medium
      - c1.xlarge
      - c3.large
      - c3.xlarge
      - c3.2xlarge
      - c3.4xlarge
      - c3.8xlarge
      - c4.large
      - c4.xlarge
      - c4.2xlarge
      - c4.4xlarge
      - c4.8xlarge
      - g2.2xlarge
      - g2.8xlarge
      - r3.large
      - r3.xlarge
      - r3.2xlarge
      - r3.4xlarge
      - r3.8xlarge
      - i2.xlarge
      - i2.2xlarge
      - i2.4xlarge
      - i2.8xlarge
      - d2.xlarge
      - d2.2xlarge
      - d2.4xlarge
      - d2.8xlarge
      - hi1.4xlarge
      - hs1.8xlarge
      - cr1.8xlarge
      - cc2.8xlarge
      - cg1.4xlarge
    ConstraintDescription: must be a valid EC2 instance type.
Resources:

  # First, a VPC:

  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: 10.1.0.0/16
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
      - Key: Name
        Value:  !Join ['', [!Ref "AWS::StackName", "-VPC" ]]
  InternetGateway:
    Type: AWS::EC2::InternetGateway
    DependsOn: VPC
  AttachGateway:
    Type: AWS::EC2::VPCGatewayAttachment
    # Notice how you can't attach an IGW to a VPC unless both are created:
    Properties:
      VpcId: !Ref VPC
      InternetGatewayId: !Ref InternetGateway
  PublicSubnetA:
  # NOTICE!: Despite their names, subnets are only "public" or "private" based on the definitions of their associated routing tables.
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      CidrBlock: 10.1.10.0/24
      MapPublicIpOnLaunch: true
      AvailabilityZone: !Select [ 0, !GetAZs ]    # Get the first AZ in the list       
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-Public-A
  PublicSubnetB:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      CidrBlock: 10.1.20.0/24
      MapPublicIpOnLaunch: true
      AvailabilityZone: !Select [ 0, !GetAZs ]    # Get the second AZ in the list 
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-Public-B
  PublicSubnetC:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      CidrBlock: 10.1.30.0/24
      MapPublicIpOnLaunch: true
      AvailabilityZone: !Select [ 0, !GetAZs ]    # Get the second AZ in the list 
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-Public-C
  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
      - Key: Name
        Value: Public
  PublicRoute1:   # Public route table has direct routing to IGW:
    Type: AWS::EC2::Route
    DependsOn: AttachGateway
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway  

  # 3 association between subnets and route table

  PublicSubnetAssociationA:
    Type:  AWS::EC2::SubnetRouteTableAssociation
    Properties:
       RouteTableId: !Ref PublicRouteTable
       SubnetId: !Ref PublicSubnetA
  PublicSubnetAssociationB:
    Type:  AWS::EC2::SubnetRouteTableAssociation
    Properties:
       RouteTableId: !Ref PublicRouteTable
       SubnetId: !Ref PublicSubnetB
  PublicSubnetAssociationC:
    Type:  AWS::EC2::SubnetRouteTableAssociation
    Properties:
       RouteTableId: !Ref PublicRouteTable
       SubnetId: !Ref PublicSubnetC

  # create a security group

  MySecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: WebSecurityGroup
      GroupName: WebSecurityGroup
      SecurityGroupIngress:
      - CidrIp: 0.0.0.0/0
        Description:  Enable HTTP access
        FromPort: 80
        IpProtocol: tcp
        ToPort: 80
      - CidrIp: 0.0.0.0/0
        Description:  Enable SSH access
        FromPort: 22
        IpProtocol: tcp
        ToPort: 22
      - CidrIp: 0.0.0.0/0
        Description:  Enable 443 access
        FromPort: 443
        IpProtocol: tcp
        ToPort: 443
      - CidrIp: 0.0.0.0/0
        Description:  Enable 8080 access
        FromPort: 8080
        IpProtocol: tcp
        ToPort: 8080
      - CidrIp: 0.0.0.0/0
        Description:  Enable 5432 access
        FromPort: 5432
        IpProtocol: tcp
        ToPort: 5432
      Tags:
      - Key: "Name"
        Value: "IADTWebSecurityGroup"
      VpcId: !Ref VPC
  # Create an instance
  MyEC2:
    Type: AWS::EC2::Instance
    Properties:
      ImageId: !Ref AmiId
      AvailabilityZone: !GetAtt PublicSubnetA.AvailabilityZone
      InstanceType: !Ref InstanceType
      KeyName: !Ref KeyName
      SecurityGroupIds:
        - !Ref MySecurityGroup
      SubnetId: !Ref PublicSubnetA
      Tags:
      - Key: "Name"
        Value: !Join ['', [!Ref "AWS::StackName", "-ec2" ]]
      UserData:
        Fn::Base64: 
          !Sub |
            #!/bin/bash -xe
            /bin/echo 'RDS_USERNAME=${RDSUsername}' >> /etc/environment
            /bin/echo 'RDS_PASSWORD=${RDSPassword}' >> /etc/environment
            /bin/echo 'RDS_DBNAME=${RDSDBName}' >> /etc/environment
            /bin/echo 'RDSHOST_NAME=${DBinstance.Endpoint.Address}' >> /etc/environment
      
  # DB Security group
  DBSecurityGroup:
    Type: AWS::RDS::DBSecurityGroup
    Properties: 
      GroupDescription: This is my db security group
      EC2VpcId: !Ref VPC
      DBSecurityGroupIngress: 
        EC2SecurityGroupId: !Ref MySecurityGroup

  # DB Instance
  DBinstance:
    Type: AWS::RDS::DBInstance
    Properties: 
      DBName: !Ref RDSDBName
      MasterUsername: !Ref RDSUsername
      MasterUserPassword: !Ref RDSPassword
      AllocatedStorage: !Ref DBAllocatedStorage
      DBSecurityGroups: 
      - !Ref DBSecurityGroup
      DBInstanceClass: !Ref DBInstanceClass
      Engine: postgres
      EngineVersion: 9.6.11
      MultiAZ: false
      PubliclyAccessible: false
      DBSubnetGroupName: !Ref DBSubnetGroup

  # DB subnet group
  DBsubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      CidrBlock: 10.1.40.0/24
      AvailabilityZone: !Select [ 0, !GetAZs ]    # Get the second AZ in the list 
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-DB-1
  DBsubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      CidrBlock: 10.1.50.0/24
      AvailabilityZone: !Select [ 1, !GetAZs ]    # Get the second AZ in the list 
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-DB-2
  # DB subnet goup
  DBSubnetGroup:
    Type: AWS::RDS::DBSubnetGroup
    Properties:
      DBSubnetGroupDescription: this is my DBSubnetGroup
      SubnetIds:
        - !Ref DBsubnet1
        - !Ref DBsubnet2
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-DBSG

  # DB route table
  DBSubnetAssociation1:
    Type:  AWS::EC2::SubnetRouteTableAssociation
    Properties:
       RouteTableId: !Ref PublicRouteTable
       SubnetId: !Ref DBsubnet1
  DBSubnetAssociation2:
    Type:  AWS::EC2::SubnetRouteTableAssociation
    Properties:
       RouteTableId: !Ref PublicRouteTable
       SubnetId: !Ref DBsubnet2

​    

